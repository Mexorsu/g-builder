import java.lang.reflect.Executable;
import java.nio.file.DirectoryIteratorException;
import java.util.HashSet;
import java.util.Queue
import java.util.Set;

import jdk.nashorn.internal.codegen.CompilationPhase
import edu.szyrek.jb.BuildPhase
import edu.szyrek.jb.BaseSettings;
import edu.szyrek.jb.api.Builder
import edu.szyrek.jb.api.Command;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.api.Target
import edu.szyrek.jb.platform.BashCommand
import edu.szyrek.jb.util.Directory
import edu.szyrek.jb.input.AppNameInput
import edu.szyrek.jb.input.CustomFlagInput;
import edu.szyrek.jb.input.IncludeInput;
import edu.szyrek.jb.input.LibraryInput;
import edu.szyrek.jb.input.LibraryPathInput;
import edu.szyrek.jb.input.ObjectFileInput
import edu.szyrek.jb.input.OutDir
import edu.szyrek.jb.input.SourceInput;
import edu.szyrek.jb.input.TargetInput;
import edu.szyrek.jb.input.In;
import edu.szyrek.jb.input.Out;
import edu.szyrek.jb.input.VersionInput;

import java.util.Collection;

import CppBuild.CppTarget;



class CppBuilder extends Builder {
	
	@Override
	public void initialize() {
		this.addPhase new CompilePhase()
		this.addPhase new LinkPhase()
	}
	
	class PrintInputs extends BuildPhase {
        @In(type=SourceInput, collection=true)
        public Set<SourceInput> sources;
		@In(type=TargetInput)
		public TargetInput target;
		@In(type=AppNameInput)
		public AppNameInput appName;
		
		@Override
		public Set<Command> execute()
		{
			def result = new HashSet<Command>();
			println "--- Inputs ---"
			println "App name: $appName"
			println "Target: $target" 
			for (Input i in sources) {
				println i
			}
			println "--------------"
			return result;
		}
	}
	
	class CompilePhase extends BuildPhase {
		@In(type=SourceInput, collection=true)
		public Set<SourceInput> sources
		@In(type=IncludeInput, collection=true)
		public Set<IncludeInput> includes
		@In(type=OutDir)
		public OutDir outDir
		@Out(type=ObjectFileInput, collection=true)
		public Set<ObjectFileInput> objects = new HashSet<>();
		
		
		@Override
		public Set<Command> execute()
		{
			def result = new HashSet<Command>();
			
			for (SourceInput source in sources) {
                if (source instanceof SourceInput){
					String objFileName = source.getValue().getLastPathCopmonent().replace(".cpp", ".o");
					String sep = File.separator;
                    String objectPath = "$outDir$sep$objFileName";
                    BashCommand command = new BashCommand("g++ -c "+source +" -o "+ objectPath + joinIncludes());
                    addObject(objectPath)
                    result.add(command);
                }
			}
			
			return result;
		}
		
		private String joinIncludes() {
			if (includes==null||includes.size()==0) return "";
			return " -I"+includes.iterator().join(" -I");
		}
		
		private void addObject(String path) {
			objects.add(new ObjectFileInput(path));
		}
	}

	
	class LinkPhase extends BuildPhase {
		@In(type=OutDir)
		public OutDir outDir;
		@In(type=TargetInput)
		public TargetInput target;
		@In(type=AppNameInput)
		public AppNameInput appName;
		@In(type=VersionInput)
		public VersionInput version;
		@In(type=ObjectFileInput, collection=true)
		public Set<ObjectFileInput> objects;
		@In(type=LibraryPathInput, collection=true)
		public Set<LibraryPathInput> libPaths
		@In(type=LibraryInput, collection=true)
		public Set<LibraryInput> libs
		
		@Override
		public Set<Command> execute()
		{
			def result = new HashSet<Command>()
			
			StringBuilder sb = new StringBuilder()
			
			if (target.getValue().equals(CppTarget.STATIC_LIBRARY)){
				String libName = "lib"+appName+"."+version+".a";
				sb.append("ar rvs "+outDir+File.separator+libName+joinLibPaths());
				for (Input objFile in objects){
					sb.append(" "+objFile)
				}
				sb.append(joinLibs());
				addArtifact(new LibraryInput(libName));
				addArtifact(new LibraryPathInput(outDir));
				
			}else if (target.getValue().equals(CppTarget.EXECUTABLE)){			
				sb.append("g++ -o "+outDir+File.separator+appName+"."+version+joinLibPaths())
				for (Input objFile in objects){
					sb.append(" "+objFile)
				}
				sb.append(joinLibs());
			}else{
				throw new IllegalArgumentException("Target not supported: $target")
			}
			BashCommand command = new BashCommand(sb.toString());
			result.add(command);
			return result;
		}
		
		private String joinLibPaths() {
			if (libPaths==null||libPaths.size()==0) return "";
			return " -L"+libPaths.iterator().join(" -L");
		}
		
		private String joinLibs() {
			if (libs==null||libs.size()==0) return "";
			return " -l"+libs.iterator().join(" -l");
		}
		
	}
	
}
