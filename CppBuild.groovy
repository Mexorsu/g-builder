import java.io.File;
import java.nio.file.DirectoryIteratorException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import edu.szyrek.jb.BaseSettings;
import edu.szyrek.jb.JBTarget;
import edu.szyrek.jb.PatternDiscovery
import edu.szyrek.jb.Result
import edu.szyrek.jb.api.Discovery;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.api.Target;
import edu.szyrek.jb.dependency.DependencyFetcher;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.Path
import edu.szyrek.jb.input.CustomFlagInput;
import edu.szyrek.jb.input.IncludeInput;
import edu.szyrek.jb.input.LibraryInput;
import edu.szyrek.jb.input.LibraryPathInput;
import edu.szyrek.jb.input.OutDir
import edu.szyrek.jb.input.SourceInput;
import edu.szyrek.jb.input.TargetInput
import edu.szyrek.jb.input.VersionInput

class CppSourceDiscovery implements Discovery {
	@Override
	public List<String> includes() {
		return Arrays.asList((".*\\.cpp"));
	}
	@Override
	public List<String> excludes() {
		return null;
	}
	@Override
	public Input<?> pathToInput(Path path) {
		return new SourceInput(path.getPath());
	}

}




class CppBuild extends BaseSettings {
	protected final static CppSourceDiscovery CPP_SOURCE = new CppSourceDiscovery();
	
	protected final static Discovery CPP_SOURCE2 = new PatternDiscovery(".*\\.cpp", "LibraryHeader.cpp"){
		@Override Input<?> pathToInput(Path p) {
			return new SourceInput(p.getPath());
		}
	};
	
	public enum CppTarget implements Target {
		STATIC_LIBRARY, DYNAMIC_LIBRARY, EXECUTABLE;
	
		@Override
		public int getCode() {
			return this.ordinal();
		}
	
	}
	public static final CppTarget LIB_S = CppTarget.STATIC_LIBRARY;
	public static final CppTarget LIB_D = CppTarget.DYNAMIC_LIBRARY;
	public static final CppTarget EXEC = CppTarget.EXECUTABLE;
	
	CppBuild(Directory buildRoot) {
		super(buildRoot);
		this.builder = "CppBuilder";
		addInput(new OutDir(buildRoot.getPath()+File.separator+"bin"));
		version(1, 0, null);
	}
	
	public Result validateSettings() {
		return new Result();
	}
	
	public void initialize() {
		
	}
	
	public void version(int major, int minor, Integer release) {
		this.addInput(new VersionInput(major, minor, release));	
	}
	
	public void addInclude(String path) {
		this.addInput(new IncludeInput(path));
	}
	public void addLibraryPath(String path) {
		this.addInput(new LibraryPathInput(path));
	}
	public void addLibrary(String name) {
		this.addInput(new LibraryInput(name));
	}
	public void addFlag(String flag) {
		this.addInput(new CustomFlagInput(flag));
	}	
	public void addStaticLib(String name, String version) {
		
	}
}
